# -*- coding: utf-8 -*-

# O objetivo desse programa eh, a partir do arquivo de tweets fornecido pelo proprio twitter,
# fornecer uma ferramenta de busca e filtragem dos links dos tweets, com opcoes de aberturas automáticas
# para acesso a pagina de um tweet específico.

import sys, csv, selenium
from os import SEEK_CUR
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import webbrowser, time, requests, json
# from urllib import parse # for separating path and hostname

selenium_time_window = 10;

def start_chrome():
	try:
		chrome = webdriver.Chrome('./chromedriver'); #TODO Atualizar!
		return chrome;
	except selenium.common.exceptions.WebDriverException:
		print ("Erro com o driver. \n\n")
		return;
	except:
		print ("Erro desconhecido.")
		return;

def start_safari():
	try:
		safari = webdriver.Safari(executable_path = '/usr/bin/safaridriver');
	except selenium.common.exceptions.SessionNotCreatedException:
		print ("Feche a atual sessão e pressione enter para continuar...");
		raw_input();
		return;
	except 'selenium.common.exceptions.SessionNotCreatedException':
		print ("Timeout.");
		return;
	return safari

def login(browser, username, password):
	#TODO: test login success
	try:
		browser.get('https://twitter.com/login');
		login_field = WebDriverWait(browser,selenium_time_window).until(EC.presence_of_element_located((By.XPATH, '//input[@name="session[username_or_email]"]')))
		login_field.clear();
		login_field.send_keys(username);
		time.sleep(1);	# Wait 1sec

		# filling password
		password_field = WebDriverWait(browser,selenium_time_window).until(EC.presence_of_element_located((By.XPATH, '//input[@name="session[password]"]')))
		password_field.clear();
		password_field.send_keys(password);

		# submit credentials
		password_field.submit()
		time.sleep(selenium_time_window/2);
	except selenium.common.exceptions.TimeoutException:
		print ("Erro no login. Tente de novo.");
		browser.quit()
		sys.exit(1)
def delete_tweets(tweets, username, password):
	#TODO: treat rate limited
	browser = start_chrome();

	if browser == None: 		# Error handling
		return
	login (browser, username, password);
	browser.get("https://twitter.com")

	with open('reprocess.js', 'w') as arq_write:
		arq_write.write("reprocess = [\n");
		for tweet in tweets:
			tweet_link = "https://twitter.com/"+username+"/status/"+tweet['tweet']['id']
			try:
				browser.get(tweet_link)
			except (selenium.common.exceptions.InvalidSessionIdException, selenium.common.exceptions.TimeoutException) as exception_timeout:
				print ("Timeout de carregamento do tweet. Fazer exclusão manual.");
				arq_write.write(json.dumps(tweet,indent=4))
				arq_write.write(',\n')
				# raw_input("Pressione enter para continuar...");
				continue

			# Tentativa de achar o botão MORE. Se não conseguir, ele vai ver se
			# o tweet não eh mais valido. Se for isso, o programa segue pro prox
			# tweet. Senão, ele tenta recarregar a página e reencontrar o botão
			try:
				more_button = WebDriverWait(browser, selenium_time_window).until(EC.element_to_be_clickable((By.CLASS_NAME, 'r-4qtqp9 r-yyyyoo r-ip8ujx r-dnmrzs r-bnwqim r-1plcrui r-lrvibr r-27tl0q'.replace(' ',"."))));
				more_button.click();
			except selenium.common.exceptions.TimeoutException:
				try:
					WebDriverWait(browser, selenium_time_window).until(EC.presence_of_element_located((By.XPATH, "//*[contains(text(),'Sorry, that page doesn’t exist!')]")));
					print ("Tweet invalido.");
					continue;
				except selenium.common.exceptions.TimeoutException:
					try:
						WebDriverWait(browser, selenium_time_window).until(EC.presence_of_element_located((By.XPATH, "//*[contains(text(),'This Tweet is unavailable')]")));
						continue;
					except:
						print ("Erro desconhecido: Botão MORE.")
						arq_write.write(json.dumps(tweet,indent=4))
						arq_write.write('\n')
						continue;
			except:
				print ("Erro desconhecido: Botão MORE.")
				arq_write.write(json.dumps(tweet,indent=4))
				arq_write.write(',\n')
				continue;

			# Tentativa de achar o botão DELETE.
			try:
				delete_button = WebDriverWait(browser, selenium_time_window).until(EC.element_to_be_clickable((By.CLASS_NAME, 'r-1loqt21 r-18u37iz r-1j3t67a r-9qu9m4 r-o7ynqc r-6416eg r-13qz1uu'.replace(' ',"."))));
				delete_button.click();
			except selenium.common.exceptions.NoSuchElementException:
				print ("Botão DELETE não encontrado. Fazer exclusão manual.");
				arq_write.write(json.dumps(tweet,indent=4))
				arq_write.write(',\n')
				continue;
			except:
				print ("Erro desconhecido: Botão DELETE.");
				arq_write.write(json.dumps(tweet,indent=4))
				arq_write.write(',\n')
				continue;
			# Tentativa de achar o botão de confirmar a deleção do tweet.
			try:
				delete_confirm = WebDriverWait(browser, selenium_time_window).until(EC.element_to_be_clickable((By.CLASS_NAME, 'css-18t94o4 css-1dbjc4n r-1dgebii r-42olwf r-sdzlij r-1phboty r-rs99b7 r-16y2uox r-1w2pmg'.replace(' ',"."))));
				# delete_confirm.click();
				print ("Tweet poderia ser deletado!!");
				time.sleep(0.5);
			except selenium.common.exceptions.NoSuchElementException:
				print ("Botão CONFIRM DELETE não encontrado. Fazer exclusão manual.");
				arq_write.write(json.dumps(tweet,indent=4))
				arq_write.write(',\n')
				continue;
			except:
				print ("Erro desconhecido: Botão CONFIRM DELETE.");
				arq_write.write(json.dumps(tweet,indent=4))
				arq_write.write(',\n')
				continue;
		arq_write.seek(-2,SEEK_CUR)
		arq_write.write('\n]')
	browser.quit();

def all_tweets_from_js(filename='tweet.js', filtro_ano='2012', filtro_mes='Jan'):
	with open(filename) as dataFile:
	    data = dataFile.read()
	    obj = data[data.find('[') : data.rfind(']')+1]
	    jsonObjs = json.loads(obj)
	tweets =[]
	for obj in jsonObjs:
		# Filtros aqui
		if obj['tweet']['created_at'][-4:]==filtro_ano and obj['tweet']['created_at'][4:7]==filtro_mes:
			tweets.append(obj);
	print ("Quantidade de tweets filtrados: ", len(tweets));
	print ("Ano: ", filtro_ano, "\t Mes:", filtro_mes)
	return tweets;

with open('auth.txt') as f:
	username=f.readline().replace('\n', '')
	password=f.readline().replace('\n', '')

if (len(sys.argv)==2):
	try:
		tweets = all_tweets_from_js(sys.argv[1]);
	except IOError:
		print ("Arquivo inexistente")
else:
	tweets = all_tweets_from_js(filtro_ano='2020', filtro_mes='Mar');

print ("O programa iniciará em 5 segundos. Aperte Ctrl+C para sair...")
time.sleep(5)
# delete_tweets(tweets,username,password)
