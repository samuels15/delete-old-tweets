# Delete old tweets
Código feito para deletar tweets antigos a partir do arquivo de histórico do Twitter.

-------------

### Motivação
A motivação inicial para o desenvolvimento desse código foi a incapacidade de ferramentas encontradas online em apagar tweets muito antigos e suas diferentes limitações.
Dessa forma, esse programa foi desenvolvido com a intenção de fornecer uma alternativa segura que pode ser executada **localmente**, isto é, na própria máquina do usuário, sem vinculação de seus dados em lugar algum, além de contornar eventuais limitações do Twitter.


------------------------
### Requisitos
* [Python](https://www.python.org) instalado (Funciona nas versões 2 e 3).
* ```pip``` instalado (normalmente vem junto com Python)
* Arquivo do histórico do [Twitter](https://help.twitter.com/pt/managing-your-account/how-to-download-your-twitter-archive)
* Editor de texto (Sublime, Atom, Gedit, Bloco de Notas)
* Driver do navegador. O programa oferece suporte para o Safari e para o Chrome. O [driver do Chrome](https://chromedriver.chromium.org/downloads) pode ser encontrado junto com esse programa, mas ele só funciona para a versão 81 do Chrome. Se esse driver for salvo na mesma pasta do programa, deve funcionar perfeitamente.

------------------
### Limitações
Assim como todos os programas, este também tem suas limitações. As que foram encontradas ~~e lembradas~~ são descritas a seguir.
* O programa NÃO é capaz de contornar a detecção de bots do Twitter. Para evitar encontrar tal detecção, ele tira um tempo entre suas operações.
* A velocidade da internet local também é uma limitação. O navegador precisa de tempo suficiente para carregar a página do Twitter referente a cada tweet. Dependendo da velocidade da sua internet, esse tempo PODE ser ajustado. Não é recomendável que ele seja REDUZIDO (pelo motivo anterior).
* O programa não é capaz de apagar retweets em perfis que são protegidos que você não siga. (Ex. Você deu RT em um tweet de um terceiro em 2012. Atualmente, o perfil desse terceiro é protegido e você NÃO o segue. O acesso ao tweet é impossibilitado e a remoção dele também.)
* Às vezes, o programa simplesmente não consegue encontrar algum botão necessário para a deleção do tweet.
* Sempre dá erro no primeiro tweet. Deixa rolar.

### Parâmetros
* Tempo entre operações
O tempo é definido pela variável ```selenium_time_window```. Todos os intervalos entre as operações são medidos proporcionalmente em relação a essa variável. Às vezes dividido por 2 ou por 5.
* Filtro de data
O filtro de data está implementado dentro da função ```all_tweets_from_js```. O filtro implementado está para tweets de 2020. A estrutura do filtro NÃO está tão clara para quem não tem experiência com programação e esse é um ponto que PODE ser melhorado.
Qualquer filtro que venha a ser implementado **deve** ser feito dentro do escopo dessa função, antes do retorno dos dados para o corpo principal do programa. Existe uma indicação do lugar correto para essas implementações.

### Reprocessamento
Quando o tweet dá erro, ele é enviado para um arquivo chamado ```reproccess.js```. Esse arquivo pode ser utilizado posteriormente para que o PRÓPRIO PROGRAMA faça um reprocessamento dos tweets que ele não foi capaz de processar.
Esse arquivo é sobrescrito toda vez que o programa roda, então isso requer atenção e cuidado por parte do operador. Um esquema melhor de reprocessamento pode ser repensado em versões futuras desse programa.

### Como rodar o programa
Baixar todos os arquivos do programa e preencher os requisitos. Dentro do arquivo de dados do Twitter, deve ser encontrado o arquivo ```tweet.js```. Esse único arquivo deve ser colocado na mesma pasta do restante dos arquivos.
Você pode fazer ajustes nos filtros diretamente no corpo do programa, conforme explicado acima.
*Recomendação: Se você tiver muitos tweets, configure os filtros para rodar a exclusão de ano a ano.*
Você deve criar um arquivo chamado auth.txt onde na PRIMEIRA LINHA deve conter seu usuário do twitter. Na segunda linha, sua senha. Esse arquivo deve ser salvo na mesma pasta do programa.
É preciso instalar as bibliotecas do Python que serão usadas:
> ```pip install -r requirements.txt```

Então o programa pode ser chamado via **Terminal** através do comando:

> ```python delete_old_tweets.py```

Após o programa terminar de rodar, você pode rodar o reprocessamento com:

> ```python delete_old_tweets.py reproccess.js```

Qualquer erro que você encontrar pode ser reportado e tentarei ajudar.

Boa sorte.
